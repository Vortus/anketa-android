package vortus.anketa;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class MainActivity extends Activity implements AdapterView.OnItemSelectedListener {

    // Variables
    private TimePicker timePicker;
    private Spinner educationSelect;
    private EditText surname;
    private TextView resultText;
    private String FILENAME = "ANKETA_FILE";

    protected void onCreate(Bundle savedInstanceState) { // On creation
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        resultTextInitialization(); // Initializing result text
        surnameInitialization(); // Initializing surname field
        spinnerInitialization(); // Initializing spinner
        timePickerInitialization();  // Initializing time picker
    }

    // Writing, saving file
    public void loadData(View view){ // Loading data
        String[] DATA = FileHandler.getData(MainActivity.this, FILENAME); // Load the data
        if(DATA == null){
            Toast.makeText(this, "Failas nerastas!", Toast.LENGTH_SHORT).show();
            return;
        }
        StringBuilder result = new StringBuilder();
        result.append("Pavard\u0117: " + DATA[0] + '\n');
        result.append("I\u0161silavinimas: " + DATA[2] + '\n');
        result.append("Keliasi: " + DATA[1] + '\n');
        resultText.setText(result.toString());
    }

    public void writeData(View view){ // Writing info to file
        if(surname.getText().toString().trim().length() == 0){ // If surname is empty
            Toast.makeText(this, "Pavard\u0117s laukelis paliktas tu\u0161\u010dias!", Toast.LENGTH_SHORT).show();
            return;
        }
        String DATA =
                surname.getText().toString() + "|" +
                timePicker.getCurrentHour() + ":" + timePicker.getCurrentMinute() + "|" +
                educationSelect.getSelectedItem().toString();
        FileHandler.rewriteData(MainActivity.this, DATA, FILENAME); // Writing data
    }

    // Initializations
    private void resultTextInitialization(){ // Initializing result text
        resultText = (TextView)findViewById(R.id.ResultView);
    }

    private void surnameInitialization(){ // Initializing surname box
        surname = (EditText)findViewById(R.id.Surname);
    }

    private void timePickerInitialization(){ // Initializing time picker
        timePicker = (TimePicker)findViewById(R.id.TimePicker);
        timePicker.setIs24HourView(true);
    }

    private void spinnerInitialization(){ // Initializing spinner
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.educations,
                android.R.layout.simple_spinner_item);
        educationSelect = (Spinner)findViewById(R.id.EducationSelect);
        educationSelect.setAdapter(adapter);
        educationSelect.setOnItemSelectedListener(this);
    }

    // Additional spinner stuff, might need sometime
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    public void onNothingSelected(AdapterView<?> parent) {

    }
}
