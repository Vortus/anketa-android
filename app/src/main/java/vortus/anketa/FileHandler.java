package vortus.anketa;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileHandler {

    public static void rewriteData(Context context, String DATA, String FILENAME){ // Rewriting DATA, to file
        try {
            FileOutputStream fos = context.openFileOutput(FILENAME, Context.MODE_PRIVATE);
            fos.write(DATA.getBytes());
            fos.close();
            Toast.makeText(context, "Duomenys \u012fra\u0161yti!", Toast.LENGTH_SHORT).show();
        } catch (FileNotFoundException e) {
            Toast.makeText(context, "Failas nerastas!", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        } catch (IOException e) {
            Toast.makeText(context, "Failo \u012fra\u0161ymo klaida!", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    public static String[] getData(Context context, String FILENAME){ // Getting data as list
        try {
            FileInputStream fis = context.openFileInput(FILENAME);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String line;

            while((line = br.readLine()) != null)
                sb.append(line);

            Toast.makeText(context, "Duomenys u\u017ekrauti!", Toast.LENGTH_SHORT).show();
            return sb.toString().split("\\|");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(context, "Failas nerastas!", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            Toast.makeText(context, "Failo \u012fra\u0161ymo klaida!", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        return null;
    }

}
